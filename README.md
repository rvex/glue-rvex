r-VEX toolchain glue
====================

This repository is used to install some so-called "glue" files that make the
various tools from the other tool repositories work together properly. This
basically amounts to a bunch of symlinks and a couple C header files for the
r-VEX architecture.


Configuration and build process
-------------------------------

Configuring and installing the glue files works like this:

 - (`rvex-cfg.py <json>`). This is automatically run by the r-VEX toolchain
   generator to configure things, but right now it's no-op.
 - `configure`. Generates a Makefile for the later steps. Allows you to set a
   custom build directory (by running the `configure` command from another
   working directory) and a custom installation directory (defaults to
   `install`).
 - (`make`). Would compile things if there were anything to compile.
 - `make install`. Copies the glue files to the specified `install` directory.

